import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./views/Login";
import Dashboard from "./views/Dashboard";
import NotFound from "./views/NotFound";

import Store from "./context/store/Store";

const App = () => {
  return (
    <Store>
      <Router>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route path="/home">
            <Dashboard />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </Router>
    </Store>
  );
};

export default App;
