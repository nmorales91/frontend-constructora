import axios from "axios";
import { environment } from "../config/config";

const axiosInstance = axios.create({
  baseURL: environment.baseUrl,
});

export default axiosInstance;
