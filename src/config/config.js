import env from "../environment";
import { getSubdomain } from "../utils/utils";

const environment = env[getSubdomain()];

export { environment };
