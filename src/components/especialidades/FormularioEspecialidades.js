import { Alert, Button, Form, Input } from "antd";
import React, { useState } from "react";
import client from "../../client/client";

const FormularioEspecialidades = ({ fetchEspecialidades }) => {
  const initialState = {
    nombre: "",
  };
  const [form] = Form.useForm();

  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const saveEspecialidad = (especialidad) => {
    setError(null);
    setSuccess(null);
    const token = localStorage.getItem("jwt");
    client
      .post("/server/especialidades", especialidad, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Especialidad ${especialidad.nombre} agregada con éxito`);
        form.setFieldsValue(initialState);
        fetchEspecialidades();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status) {
            setError(error.response.data.message);
          } else {
            setError("Ocurrió un error al agregar la especialidad");
          }
        } else {
          setError(error.message);
        }
      });
  };
  return (
    <Form
      onFinish={saveEspecialidad}
      initialValues={initialState}
      form={form}
      layout="horizontal"
    >
      <Form.Item
        label="Nombre"
        name="nombre"
        rules={[{ required: true, message: "Ingrese un nombre" }]}
      >
        <Input autoComplete="off" />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          style={{ marginBottom: 10, width: "100%" }}
        >
          Guardar
        </Button>
        {error ? <Alert message={error} type="error" /> : null}
        {success ? <Alert message={success} type="success" /> : null}
      </Form.Item>
    </Form>
  );
};

export default FormularioEspecialidades;
