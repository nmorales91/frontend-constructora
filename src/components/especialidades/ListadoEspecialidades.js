import { Table } from "antd";

const ListadoEspecialidades = ({ especialidades, load }) => {
  const columns = [
    { title: "Especialidad", dataIndex: "nombre", key: "nombre" },
  ];

  return <Table loading={load} dataSource={especialidades} columns={columns} />;
};

export default ListadoEspecialidades;
