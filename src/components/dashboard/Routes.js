import { Route, Switch } from "react-router-dom";
import Usuarios from "../../views/content/usuarios/Usuarios";
import EditarUsuario from "../../views/content/usuarios/Editar";
import Constructoras from "../../views/content/constructoras/Constructoras";
import ListadoUsuarios from "../../views/content/usuarios/ListadoUsuarios";
import Especialidades from "../../views/content/especialidades/Especialidades";
import Roles from "../../views/content/roles/Roles";
import ListadoConstructoras from "../../views/content/constructoras/ListadoConstructoras";
import ListadoRepresentantes from "../../views/content/representantes/ListadoRepresentantes";
import Representantes from "../../views/content/representantes/Representantes";
import Contratistas from "../../views/content/contratistas/Contratistas";
import ListadoContratistas from "../../views/content/contratistas/ListadoContratistas";
import Personal from "../../views/content/personal/Personal";
import ListadoPersonal from "../../views/content/personal/ListadoPersonal";
import Trabajadores from "../../views/content/trabajadores/Trabajadores";
import ListadoTrabajadores from "../../views/content/trabajadores/ListadoTrabajadores";
import EditarConstructora from "../../views/content/constructoras/Editar";
import EditarContratista from "../../views/content/contratistas/Editar";
import EditarTrabajadores from "../../views/content/trabajadores/Editar";
import EditarPersonal from "../../views/content/personal/Editar";
import MarcarAsistencia from "../../views/content/asistencia/MarcarAsistencia";

const Routes = ({ path }) => {
  return (
    <Switch>
      <Route exact path={path}>
        <h3>Dashboard.</h3>
      </Route>
      <Route exact path={`${path}/usuario`}>
        <Usuarios />
      </Route>
      <Route path={`${path}/usuario/:id`}>
        <EditarUsuario />
      </Route>
      <Route path={`${path}/usuarios`}>
        <ListadoUsuarios />
      </Route>
      <Route path={`${path}/especialidades`}>
        <Especialidades />
      </Route>
      <Route path={`${path}/roles`}>
        <Roles />
      </Route>
      <Route exact path={`${path}/constructora`}>
        <Constructoras />
      </Route>
      <Route path={`${path}/constructora/:id`}>
        <EditarConstructora />
      </Route>
      <Route path={`${path}/constructoras`}>
        <ListadoConstructoras />
      </Route>
      <Route path={`${path}/representante`}>
        <Representantes />
      </Route>
      <Route path={`${path}/representantes`}>
        <ListadoRepresentantes />
      </Route>
      <Route exact path={`${path}/contratista`}>
        <Contratistas />
      </Route>
      <Route path={`${path}/contratista/:id`}>
        <EditarContratista />
      </Route>
      <Route path={`${path}/contratistas`}>
        <ListadoContratistas />
      </Route>
      <Route exact path={`${path}/persona`}>
        <Personal />
      </Route>
      <Route path={`${path}/personal/:id`}>
        <EditarPersonal />
      </Route>
      <Route path={`${path}/personal`}>
        <ListadoPersonal />
      </Route>
      <Route exact path={`${path}/trabajador`}>
        <Trabajadores />
      </Route>
      <Route path={`${path}/trabajador/:id`}>
        <EditarTrabajadores />
      </Route>
      <Route path={`${path}/trabajadores`}>
        <ListadoTrabajadores />
      </Route>
      <Route path={`${path}/asistencia`}>
        <MarcarAsistencia />
      </Route>
      <Route path={`${path}/*`}>
        <h3>404.</h3>
      </Route>
    </Switch>
  );
};
export default Routes;
