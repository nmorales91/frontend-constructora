import {
  BarChartOutlined,
  UploadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";
import { Layout, Menu } from "antd";
const { Sider } = Layout;
const { SubMenu } = Menu;

const LateralMenu = ({ url }) => {
  return (
    <Sider
      style={{
        overflow: "auto",
        height: "100vh",
        position: "fixed",
        left: 0,
      }}
    >
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1" icon={<UserOutlined />}>
          <Link to={`${url}`}>Inicio</Link>
        </Menu.Item>
        <SubMenu key="sub1" icon={<UserOutlined />} title="Usuarios">
          <Menu.Item key="2">
            <Link to={`${url}/usuario`}>Agregar Usuarios</Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to={`${url}/usuarios`}>Listado Usuarios</Link>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="4" icon={<UploadOutlined />}>
          <Link to={`${url}/roles`}>Roles</Link>
        </Menu.Item>
        <Menu.Item key="5" icon={<BarChartOutlined />}>
          <Link to={`${url}/especialidades`}>Especialidades</Link>
        </Menu.Item>
        <SubMenu key="sub2" icon={<UserOutlined />} title="Constructoras">
          <Menu.Item key="6">
            <Link to={`${url}/constructora`}>Agregar Constructora</Link>
          </Menu.Item>
          <Menu.Item key="7">
            <Link to={`${url}/constructoras`}>Listado Constructoras</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub3" icon={<UserOutlined />} title="Representantes">
          <Menu.Item key="8">
            <Link to={`${url}/representante`}>Agregar Representante</Link>
          </Menu.Item>
          <Menu.Item key="9">
            <Link to={`${url}/representantes`}>Listado Representantes</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub4" icon={<UserOutlined />} title="Contratistas">
          <Menu.Item key="10">
            <Link to={`${url}/contratista`}>Agregar Contratista</Link>
          </Menu.Item>
          <Menu.Item key="11">
            <Link to={`${url}/contratistas`}>Listado Contratistas</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub5" icon={<UserOutlined />} title="Personal">
          <Menu.Item key="12">
            <Link to={`${url}/persona`}>Agregar Personal</Link>
          </Menu.Item>
          <Menu.Item key="13">
            <Link to={`${url}/personal`}>Listado Personal</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub6" icon={<UserOutlined />} title="Trabajadores">
          <Menu.Item key="14">
            <Link to={`${url}/trabajador`}>Agregar Trabajador</Link>
          </Menu.Item>
          <Menu.Item key="15">
            <Link to={`${url}/trabajadores`}>Listado Trabajadores</Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub7" icon={<UserOutlined />} title="Asistencia">
          <Menu.Item key="6">
            <Link to={`${url}/asistencia`}>Marcar Asistencia</Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  );
};
export default LateralMenu;
