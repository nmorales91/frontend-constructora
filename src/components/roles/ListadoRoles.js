import { Table } from "antd";

const ListadoRoles = ({ roles, load }) => {
  const columns = [{ title: "Rol", dataIndex: "nombre", key: "nombre" }];

  return <Table loading={load} dataSource={roles} columns={columns} />;
};

export default ListadoRoles;
