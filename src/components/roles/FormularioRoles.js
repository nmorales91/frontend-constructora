import { Alert, Button, Form, Input } from "antd";
import React, { useState } from "react";
import client from "../../client/client";

const FormularioRoles = ({ fetchRoles }) => {
  const initialState = {
    nombre: "",
  };
  const [form] = Form.useForm();

  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const saveRol = (rol) => {
    setError(null);
    setSuccess(null);
    const token = localStorage.getItem("jwt");
    client
      .post("/server/roles", rol, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Rol ${rol.nombre} agregado con éxito`);
        form.setFieldsValue(initialState);
        fetchRoles();
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status) {
            setError(error.response.data.message);
          } else {
            setError("Ocurrió un error al agregar el rol");
          }
        } else {
          setError(error.message);
        }
      });
  };
  return (
    <Form
      onFinish={saveRol}
      initialValues={initialState}
      form={form}
      layout="horizontal"
    >
      <Form.Item
        label="Nombre"
        name="nombre"
        rules={[{ required: true, message: "Ingrese un nombre" }]}
      >
        <Input autoComplete="off" />
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          style={{ marginBottom: 10, width: "100%" }}
        >
          Guardar
        </Button>
        {error ? <Alert message={error} type="error" /> : null}
        {success ? <Alert message={success} type="success" /> : null}
      </Form.Item>
    </Form>
  );
};

export default FormularioRoles;
