import React, { useState } from "react";
import { Alert, Input } from "antd";
import client from "../../client/client";
const { Search } = Input;

const Asistencia = ({ setAsistencia }) => {
  const [asistenciaInput, setAsistenciaInput] = useState("");
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const token = localStorage.getItem("jwt");
  const saveAsistencia = (value) => {
    setError(null);
    setSuccess(null);
    client
      .post(
        "/server/asistencia",
        { code: value },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((response) => {
        setSuccess(response.data.asistencia.message);
        setAsistencia(response.data.asistencia.data);
      })
      .catch((error) => {
        setError(error.response.data.message);
      });
    setAsistenciaInput("");
  };
  return (
    <>
      <h2>Asistencia</h2>
      <Search
        placeholder="Ingrese código"
        allowClear
        enterButton="Marcar"
        size="large"
        onSearch={saveAsistencia}
        style={{ marginBottom: 20 }}
        onChange={(e) => setAsistenciaInput(e.target.value)}
        value={asistenciaInput}
      />
      {error ? <Alert message={error} type="error" /> : null}
      {success ? <Alert message={success} type="success" /> : null}
    </>
  );
};

export default Asistencia;
