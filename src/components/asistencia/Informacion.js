import React, { useEffect, useState } from "react";
import { Table } from "antd";
import client from "../../client/client";

const columns = [
  {
    title: "Tipo",
    dataIndex: "tipo",
    key: "tipo",
  },
  {
    title: "Hora",
    dataIndex: "hora",
    key: "hora",
  },
];
const Informacion = ({ asistencia }) => {
  const [horas, setHoras] = useState(null);
  const [usuario, setUsuario] = useState(null);
  const [idUsuario, setIdUsuario] = useState(null);
  useEffect(() => {
    setHoras(asistencia ? asistencia.horas : null);
    setIdUsuario(asistencia ? asistencia.id_usuario : null);
  }, [asistencia]);

  useEffect(() => {
    if(usuario){
      getUsuario(idUsuario);
    }
  }, [idUsuario]);

  const getUsuario = (idUsuario) => {
    const token = localStorage.getItem("jwt");
    client
      .get(`/server/trabajadores/${idUsuario}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setUsuario({
          nombre: response.data.data.nombre,
          apellido: response.data.data.apellido_paterno,
        });
      });
  };
  return (
    <>
      <h2>Información</h2>
      <h4>{usuario ? `${usuario.nombre} ${usuario.apellido}` : null}</h4>
      <Table columns={columns} dataSource={horas} />
    </>
  );
};

export default Informacion;
