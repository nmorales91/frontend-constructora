import moment from 'moment';
import 'moment/locale/es-mx';

const HeaderAsistencia = () => {
	return(<h2 style={{margin:20}}>{moment().format("dddd D [de] MMMM [del] YYYY").toUpperCase()}</h2>);
}

export default HeaderAsistencia;
