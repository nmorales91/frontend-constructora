const useAutoCompleteEmail = (value) => {
  let res = [];
  if (!value || value.indexOf("@") >= 0) {
    res = [];
  } else {
    res = ["gmail.com", "hotmail.cl", "hotmail.com"].map((domain) => ({
      value: `${value}@${domain}`,
    }));
  }
  return res;
};

export default useAutoCompleteEmail;
