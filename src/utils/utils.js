const getSubdomain = () => {
  const url = window.location.host;
  if (url.includes("nicolasmorales.dev")) {
    const splitUrl = url.split(".");
    if (splitUrl.length > 2) {
      return splitUrl[0];
    } else {
      return "production";
    }
  } else {
    return process.env.REACT_APP_ENVIRONMENT || "dev";
  }
};

const getColor = (env) => {
  switch (env) {
    case "develop":
      return "#712929de";
    case "staging":
      return "#0d1d1c";
    case "production":
      return "#282c34";
    default:
      return "#282c34";
  }
};

export { getSubdomain, getColor };
