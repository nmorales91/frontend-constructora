import React, { useContext, useLayoutEffect } from "react";
import "../styles/Dashboard.css";
import { StoreGlobal } from "../context/store/Store";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Layout } from "antd";
import LateralMenu from "../components/dashboard/LateralMenu";
import Routes from "../components/dashboard/Routes";

const { Header, Content, Footer } = Layout;

const Dashboard = () => {
  const context = useContext(StoreGlobal);
  let { path, url } = useRouteMatch();

  // redirect auth
  let history = useHistory();
  useLayoutEffect(() => {
    if (!context.stateUser.isAuthenticated) {
      history.push("/");
    }
  }, [context.stateUser.isAuthenticated, history]);
  // redirect auth

  return (
    <Layout>
      <LateralMenu url={url} />
      <Layout
        className="site-layout"
        style={{ marginLeft: 200, height: "100vh" }}
      >
        <Header className="site-layout-background" style={{ padding: 0 }} />
        <Content
          style={{
            margin: "24px 16px 0",
            overflow: "initial",
            minHeight: "auto",
          }}
        >
          <div
            className="site-layout-background"
            style={{ padding: 24, textAlign: "center", height: "100%" }}
          >
            <Routes path={path} />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Tesis Constructora ©2021 por Nicolás Morales
        </Footer>
      </Layout>
    </Layout>
  );
};

export default Dashboard;
