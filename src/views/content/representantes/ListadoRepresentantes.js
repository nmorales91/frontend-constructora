import React, { useEffect, useState } from "react";
import client from "../../../client/client";
import { Table } from "antd";

const columns = [
  { title: "Rut", dataIndex: "rut", key: "rut" },
  { title: "Nombre", dataIndex: "nombre", key: "nombre" },
  { title: "Dirección", dataIndex: "direccion", key: "direccion" },
  { title: "Ciudad", dataIndex: "ciudad", key: "ciudad" },
  { title: "Comuna", dataIndex: "comuna", key: "comuna" },
  { title: "Correo", dataIndex: "correo", key: "correo" },
  {
    title: "Editar",
    dataIndex: "",
    key: "x",
    render: () => <a>Editar</a>,
  },
];

const ListadoRepresentantes = () => {
  const [representantes, setRepresentantes] = useState([]);
  const [load, setLoad] = useState(true);
  useEffect(() => {
    async function fetchConstructoras() {
      const token = localStorage.getItem("jwt");
      const result = await client.get("/server/representantes", {
        headers: { Authorization: `Bearer ${token}` },
      });
      setLoad(false);
      setRepresentantes(result.data.data);
    }
    fetchConstructoras();
  }, []);

  return <Table loading={load} dataSource={representantes} columns={columns} />;
};

export default ListadoRepresentantes;
