import React, { useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Alert,
  Empty,
  DatePicker,
} from "antd";
import client from "../../../client/client";
import moment from "moment";
import { useParams } from "react-router-dom";
import isEmpty from "../../../context/validations/isEmpty";

const EditarPersonal = () => {
  const token = localStorage.getItem("jwt");
  const { id } = useParams();
  const [form] = Form.useForm();

  const [constructoras, setConstructoras] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const savePersonal = (personal) => {
    setError(null);
    setSuccess(null);
    client
      .put(`/server/personal/${id}`, personal, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Personal ${personal.nombre} agregado con éxito`);
      })
      .catch((error) => {
        if (error.response.status) {
          let errormsg = error.response.data.message;
          if (errormsg.includes("duplicate key")) {
            errormsg = "Este rut ya está siendo utilizado";
          }
          setError(errormsg);
        } else {
          setError("Ocurrió un error al agregar el personal");
        }
      });
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  async function fetchConstructoras() {
    const constructoras = await client.get("/server/constructoras", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setConstructoras(constructoras.data.data);
  }

  async function fetchPersonal() {
    const personal = await client.get(`/server/personal/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    const personalForm = {
      ...personal.data.data,
      id_constructora: personal.data.data.constructora._id,
      fecha_nacimiento: !isEmpty(personal.data.data.fecha_nacimiento)
        ? moment(personal.data.data.fecha_nacimiento)
        : null,
      fecha_ingreso: moment(personal.data.data.fecha_ingreso),
      duracion_contrato: !isEmpty(personal.data.data.duracion_contrato)
        ? moment(personal.data.data.duracion_contrato)
        : null,
    };
    form.setFieldsValue(personalForm);
  }

  useEffect(() => {
    fetchConstructoras();
    fetchPersonal();
  }, []);

  return (
    <Form onFinish={savePersonal} form={form} {...layout} layout="horizontal">
      <Row>
        <Col span={12}>
          <Form.Item
            label="Rut"
            name="rut"
            rules={[{ required: true, message: "Ingrese el rut" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Apellido Paterno"
            name="apellido_paterno"
            rules={[{ required: true, message: "Ingrese un apellido" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Apellido Materno" name="apellido_materno">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Fecha Nacimiento" name="fecha_nacimiento">
            <DatePicker style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item label="Dirección" name="direccion">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Ciudad" name="ciudad">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Nacionalidad" name="nacionalidad">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Fecha Ingreso"
            name="fecha_ingreso"
            rules={[{ required: true, message: "Ingrese una fecha" }]}
          >
            <DatePicker style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item label="Estado Civil" name="estado_civil">
            <Input autoComplete="off" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Cargo" name="cargo">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Sueldo" name="sueldo">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="AFP" name="afp">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Salud" name="salud">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Duración Contrato" name="duracion_contrato">
            <DatePicker style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item label="Teléfono" name="telefono">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Medio de Pago" name="medio_pago">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Escolaridad" name="escolaridad">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Constructora"
            name="id_constructora"
            rules={[{ required: true, message: "Seleccione una constructora" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Constructoras"}
                />
              }
            >
              {constructoras.map((constructora) => {
                return (
                  <Select.Option
                    key={constructora._id}
                    value={constructora._id}
                  >
                    {constructora.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
export default EditarPersonal;
