import React, { useEffect, useState } from "react";
import client from "../../../client/client";
import { Table } from "antd";
import {Link} from "react-router-dom";

const columns = [
  { title: "Rut", dataIndex: "rut", key: "rut" },
  {
    title: "Nombre",
    dataIndex: "nombre",
    key: "nombre",
    render: (text, record) => {
      return `${text} ${record.apellido_paterno}`;
    },
  },
  {
    title: "Fecha Nacimiento",
    dataIndex: "fecha_nacimiento",
    key: "fecha_nacimiento",
    render: (fecha) => {
      let date = new Date(fecha);
      return (
        <span>
          {date.toLocaleString("es-CL", {
            timeZone: "UTC",
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
          })}
        </span>
      );
    },
  },
  { title: "Dirección", dataIndex: "direccion", key: "direccion" },
  { title: "Ciudad", dataIndex: "ciudad", key: "ciudad" },
  { title: "Nacionalidad", dataIndex: "nacionalidad", key: "nacionalidad" },
  {
    title: "Fecha Ingreso",
    dataIndex: "fecha_ingreso",
    key: "fecha_ingreso",
    render: (fecha) => {
      let date = new Date(fecha);
      return (
        <span>
          {date.toLocaleString("es-CL", {
            timeZone: "UTC",
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
          })}
        </span>
      );
    },
  },
  { title: "Cargo", dataIndex: "cargo", key: "cargo" },
  { title: "Sueldo", dataIndex: "sueldo", key: "sueldo" },
  {
    title: "Duración Contrato",
    dataIndex: "duracion_contrato",
    key: "duracion_contrato",
    render: (fecha) => {
      let date = new Date(fecha);
      return (
        <span>
          {date.toLocaleString("es-CL", {
            timeZone: "UTC",
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
          })}
        </span>
      );
    },
  },
  { title: "Teléfono", dataIndex: "telefono", key: "telefono" },
  {
    title: "Constructora",
    dataIndex: "constructora",
    key: "constructora",
    render: (constructora) => {
      return <span>{constructora.nombre}</span>;
    },
  },
  { title: "Código", dataIndex: "codigo", key: "codigo" },
  {
    title: "Editar",
    dataIndex: "",
    key: "x",
    render: (text, record) => {
      return <Link to={`personal/${record._id}`}>Editar</Link>;
    },
  },
];

const ListadoPersonal = () => {
  const [personal, setPersonal] = useState([]);
  const [load, setLoad] = useState(true);
  useEffect(() => {
    async function fetchPersonal() {
      const token = localStorage.getItem("jwt");
      const result = await client.get("/server/personal", {
        headers: { Authorization: `Bearer ${token}` },
      });
      setLoad(false);
      setPersonal(result.data.data);
    }
    fetchPersonal();
  }, []);

  return <Table loading={load} dataSource={personal} columns={columns} />;
};

export default ListadoPersonal;
