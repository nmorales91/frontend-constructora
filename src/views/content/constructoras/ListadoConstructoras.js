import React, { useEffect, useState } from "react";
import client from "../../../client/client";
import { Table } from "antd";
import {Link} from "react-router-dom";

const columns = [
  { title: "Rut", dataIndex: "rut", key: "rut" },
  { title: "Nombre", dataIndex: "nombre", key: "nombre" },
  { title: "Giro", dataIndex: "giro", key: "giro" },
  { title: "Dirección", dataIndex: "direccion", key: "direccion" },
  { title: "Ciudad", dataIndex: "ciudad", key: "ciudad" },
  { title: "Correo", dataIndex: "correo", key: "correo" },
  {
    title: "Editar",
    dataIndex: "",
    key: "x",
    render: (text, record) => {
      return <Link to={`constructora/${record._id}`}>Editar</Link>
    },
  },
];

const ListadoConstructoras = () => {
  const [constructoras, setConstructoras] = useState([]);
  const [load, setLoad] = useState(true);
  useEffect(() => {
    async function fetchConstructoras() {
      const token = localStorage.getItem("jwt");
      const result = await client.get("/server/constructoras", {
        headers: { Authorization: `Bearer ${token}` },
      });
      setLoad(false);
      setConstructoras(result.data.data);
    }
    fetchConstructoras();
  }, []);

  return <Table loading={load} dataSource={constructoras} columns={columns} />;
};

export default ListadoConstructoras;
