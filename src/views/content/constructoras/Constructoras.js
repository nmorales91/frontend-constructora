import React, { useState } from "react";
import { Form, Input, Row, Col, AutoComplete, Button, Alert } from "antd";

import autoCompleteEmail from "../../../utils/autocompleteEmail";
import client from "../../../client/client";

const Constructoras = () => {
  const initialState = {
    rut: "",
    nombre: "",
    giro: "",
    direccion: "",
    ciudad: "",
    comuna: "",
    region: "",
    telefono: "",
    correo: "",
  };

  const [form] = Form.useForm();

  const [result, setResult] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const handleSearch = (value) => {
    setResult(autoCompleteEmail(value));
  };

  const saveConstructora = (constructora) => {
    setError(null);
    setSuccess(null);
    const token = localStorage.getItem("jwt");
    client
      .post("/server/constructoras", constructora, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Constructora ${constructora.nombre} agregada con éxito`);
        form.setFieldsValue(initialState);
      })
      .catch((error) => {
        if (error.response.status) {
          setError(error.response.data.message);
        } else {
          setError("Ocurrió un error al agregar la constructora");
        }
      });
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  return (
    <Row>
      <Col span={10} offset={2}>
        <Form
          onFinish={saveConstructora}
          initialValues={initialState}
          form={form}
          {...layout}
          layout="horizontal"
        >
          <Form.Item
            label="Rut"
            name="rut"
            rules={[{ required: true, message: "Ingrese un rut" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Giro" name="giro">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Dirección" name="direccion">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Ciudad" name="ciudad">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Comuna" name="comuna">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Región" name="region">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Teléfono" name="telefono">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Correo"
            name="correo"
            rules={[{ required: true, message: "Ingrese su correo" }]}
          >
            <AutoComplete onSearch={handleSearch} options={result}>
              <Input autoComplete="off" />
            </AutoComplete>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
export default Constructoras;
