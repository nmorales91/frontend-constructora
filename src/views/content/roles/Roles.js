import { Row, Col } from "antd";
import React, { useState, useEffect } from "react";
import FormularioRoles from "../../../components/roles/FormularioRoles";
import ListadoRoles from "../../../components/roles/ListadoRoles";
import client from "../../../client/client";

const Roles = () => {
  const [roles, setRoles] = useState([]);
  const [load, setLoad] = useState(false);
  async function fetchRoles(first) {
    if (first) {
      setLoad(true);
    }
    const token = localStorage.getItem("jwt");
    const result = await client.get("/server/roles", {
      headers: { Authorization: `Bearer ${token}` },
    });
    if (first) {
      setLoad(false);
    }
    setRoles(result.data.data);
  }

  useEffect(() => {
    fetchRoles(true);
  }, []);

  return (
    <Row>
      <Col span={8} offset={1}>
        <FormularioRoles fetchRoles={fetchRoles} />
      </Col>
      <Col span={8} offset={3}>
        <ListadoRoles load={load} roles={roles} />
      </Col>
    </Row>
  );
};

export default Roles;
