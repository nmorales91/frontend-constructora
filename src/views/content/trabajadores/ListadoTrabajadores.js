import React, { useEffect, useState } from "react";
import client from "../../../client/client";
import { Table } from "antd";
import {Link} from "react-router-dom";

const columns = [
  { title: "Rut", dataIndex: "rut", key: "rut" },
  {
    title: "Nombre",
    dataIndex: "nombre",
    key: "nombre",
    render: (text, record) => {
      return `${text} ${record.apellido_paterno}`;
    },
  },
  {
    title: "Fecha Nacimiento",
    dataIndex: "fecha_nacimiento",
    key: "fecha_nacimiento",
    render: (fecha) => {
      let date = new Date(fecha);
      return (
        <span>
          {date.toLocaleString("es-CL", {
            timeZone: "UTC",
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
          })}
        </span>
      );
    },
  },
  { title: "Dirección", dataIndex: "direccion", key: "direccion" },
  { title: "Ciudad", dataIndex: "ciudad", key: "ciudad" },
  { title: "Comuna", dataIndex: "comuna", key: "comuna" },
  { title: "Nacionalidad", dataIndex: "nacionalidad", key: "nacionalidad" },
  {
    title: "Contratista",
    dataIndex: "contratista",
    key: "contratista",
    render: (contratista) => {
      return <span>{contratista.nombre}</span>;
    },
  },
  {
    title: "Constructora",
    dataIndex: "constructora",
    key: "constructora",
    render: (constructora) => {
      return <span>{constructora.nombre}</span>;
    },
  },
  { title: "Código", dataIndex: "codigo", key: "codigo" },
  {
    title: "Editar",
    dataIndex: "",
    key: "x",
    render: (text, record) => {
      return <Link to={`trabajador/${record._id}`}>Editar</Link>;
    },
  },
];

const ListadoTrabajadores = () => {
  const [trabajadores, setTrabajadores] = useState([]);
  const [load, setLoad] = useState(true);
  useEffect(() => {
    async function fetchTrabajadores() {
      const token = localStorage.getItem("jwt");
      const result = await client.get("/server/trabajadores", {
        headers: { Authorization: `Bearer ${token}` },
      });
      setLoad(false);
      setTrabajadores(result.data.data);
    }
    fetchTrabajadores();
  }, []);

  return <Table loading={load} dataSource={trabajadores} columns={columns} />;
};

export default ListadoTrabajadores;
