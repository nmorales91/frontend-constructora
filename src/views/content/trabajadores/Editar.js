import React, { useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Alert,
  Empty,
  DatePicker,
} from "antd";
import client from "../../../client/client";
import { useParams } from "react-router-dom";
import moment from "moment";
import isEmpty from "../../../context/validations/isEmpty";

const EditarTrabajadores = () => {
  const token = localStorage.getItem("jwt");
  const { id } = useParams();
  const [form] = Form.useForm();

  const [contratistas, setContratistas] = useState([]);
  const [constructoras, setConstructoras] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const editTrabajador = (trabajador) => {
    setError(null);
    setSuccess(null);
    client
      .put(`/server/trabajadores/${id}`, trabajador, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Trabajador ${trabajador.nombre} editado con éxito`);
      })
      .catch((error) => {
        if (error.response.status) {
          let errormsg = error.response.data.message;
          if (errormsg.includes("duplicate key")) {
            errormsg = "Este rut ya está siendo utilizado";
          }
          setError(errormsg);
        } else {
          setError("Ocurrió un error al editar el trabajador");
        }
      });
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  async function fetchConstructoras() {
    const constructoras = await client.get("/server/constructoras", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setConstructoras(constructoras.data.data);
  }

  async function fetchContratistas() {
    const contratistas = await client.get("/server/contratistas", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setContratistas(contratistas.data.data);
  }

  async function fetchTrabajador() {
    const trabajador = await client.get(`/server/trabajadores/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    const trabajadorForm = {
      ...trabajador.data.data,
      id_constructora: trabajador.data.data.constructora._id,
      id_contratista: trabajador.data.data.contratista._id,
      fecha_nacimiento: !isEmpty(trabajador.data.data.fecha_nacimiento) ? moment(trabajador.data.data.fecha_nacimiento) : null,
    };
    form.setFieldsValue(trabajadorForm);
  }

  useEffect(() => {
    fetchConstructoras();
    fetchContratistas();
    fetchTrabajador();
  }, []);

  return (
    <Form onFinish={editTrabajador} form={form} {...layout} layout="horizontal">
      <Row>
        <Col span={12}>
          <Form.Item
            label="Rut"
            name="rut"
            rules={[{ required: true, message: "Ingrese el rut" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Apellido Paterno"
            name="apellido_paterno"
            rules={[{ required: true, message: "Ingrese un apellido" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Apellido Materno" name="apellido_materno">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Fecha Nacimiento" name="fecha_nacimiento">
            <DatePicker style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item label="Dirección" name="direccion">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Ciudad" name="ciudad">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Comuna" name="comuna">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Región" name="region">
            <Input autoComplete="off" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Vehículo" name="vehiculo">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Nacionalidad" name="nacionalidad">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Contratista"
            name="id_contratista"
            rules={[{ required: true, message: "Seleccione un contratista" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Contratistas"}
                />
              }
            >
              {contratistas.map((contratista) => {
                return (
                  <Select.Option key={contratista._id} value={contratista._id}>
                    {contratista.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label="Constructora"
            name="id_constructora"
            rules={[{ required: true, message: "Seleccione una constructora" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Constructoras"}
                />
              }
            >
              {constructoras.map((constructora) => {
                return (
                  <Select.Option
                    key={constructora._id}
                    value={constructora._id}
                  >
                    {constructora.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
export default EditarTrabajadores;
