import React, { useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Button,
  Alert,
  Empty,
} from "antd";
import client from "../../../client/client";

const Contratistas = () => {
  const token = localStorage.getItem("jwt");
  const initialState = {
    rut: "",
    nombre: "",
    id_especialidad: "",
    id_representante: "",
  };

  const [form] = Form.useForm();

  const [especialidades, setEspecialidades] = useState([]);
  const [representantes, setRepresentantes] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const saveContratista = (contratista) => {
    setError(null);
    setSuccess(null);
    client
      .post("/server/contratistas", contratista, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Contratista ${contratista.nombre} agregado con éxito`);
        form.setFieldsValue(initialState);
      })
      .catch((error) => {
        if (error.response.status) {
          setError(error.response.data.message);
        } else {
          setError("Ocurrió un error al agregar el contratista");
        }
      });
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  async function fetchEspecialidades() {
    const especialidades = await client.get("/server/especialidades", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setEspecialidades(especialidades.data.data);
  }
  async function fetchRepresentantes() {
    const representantes = await client.get("/server/representantes", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setRepresentantes(representantes.data.data);
  }

  useEffect(() => {
    fetchEspecialidades();
    fetchRepresentantes();
  }, []);

  return (
    <Row>
      <Col span={8} offset={2}>
        <Form
          onFinish={saveContratista}
          initialValues={initialState}
          form={form}
          {...layout}
          layout="horizontal"
        >
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Rut"
            name="rut"
            rules={[{ required: true, message: "Ingrese el rut" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Especialidad"
            name="id_especialidad"
            rules={[{ required: true, message: "Seleccione una especialidad" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Especialidades"}
                />
              }
            >
              {especialidades.map((especialidad) => {
                return (
                  <Select.Option
                    key={especialidad._id}
                    value={especialidad._id}
                  >
                    {especialidad.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label="Representante"
            name="id_representante"
            rules={[{ required: true, message: "Seleccione un representante" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Representantes"}
                />
              }
            >
              {representantes.map((representante) => {
                return (
                  <Select.Option
                    key={representante._id}
                    value={representante._id}
                  >
                    {representante.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
export default Contratistas;
