import React, { useEffect, useState } from "react";
import client from "../../../client/client";
import { Table } from "antd";
import { Link } from "react-router-dom";

const columns = [
  { title: "Nombre", dataIndex: "nombre", key: "nombre" },
  { title: "Rut", dataIndex: "rut", key: "rut" },
  {
    title: "Especialidad",
    dataIndex: "especialidad",
    key: "especialidad",
    render: (especialidad) => {
      return <span>{especialidad.nombre}</span>;
    },
  },
  {
    title: "Representante",
    dataIndex: "representante",
    key: "representante",
    render: (representante) => {
      return <span>{representante.nombre}</span>;
    },
  },
  {
    title: "Editar",
    dataIndex: "",
    key: "x",
    render: (text, record) => {
      return <Link to={`contratista/${record._id}`}>Editar</Link>;
    },
  },
];

const ListadoContratistas = () => {
  const [contratistas, setContratistas] = useState([]);
  const [load, setLoad] = useState(true);
  useEffect(() => {
    async function fetchContratistas() {
      const token = localStorage.getItem("jwt");
      const result = await client.get("/server/contratistas", {
        headers: { Authorization: `Bearer ${token}` },
      });
      setLoad(false);
      setContratistas(result.data.data);
    }
    fetchContratistas();
  }, []);

  return <Table loading={load} dataSource={contratistas} columns={columns} />;
};

export default ListadoContratistas;
