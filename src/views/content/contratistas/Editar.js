import React, { useState, useEffect } from "react";
import { Form, Input, Select, Row, Col, Button, Alert, Empty } from "antd";
import client from "../../../client/client";
import { useParams } from "react-router-dom";

const EditarContratista = () => {
  const token = localStorage.getItem("jwt");
  const { id } = useParams();
  const [form] = Form.useForm();

  const [especialidades, setEspecialidades] = useState([]);
  const [representantes, setRepresentantes] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const editContratista = (contratista) => {
    setError(null);
    setSuccess(null);
    client
      .put(`/server/contratistas/${id}`, contratista, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Contratista ${contratista.nombre} editado con éxito`);
      })
      .catch((error) => {
        if (error.response.status) {
          setError(error.response.data.message);
        } else {
          setError("Ocurrió un error al agregar el contratista");
        }
      });
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  async function fetchEspecialidades() {
    const especialidades = await client.get("/server/especialidades", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setEspecialidades(especialidades.data.data);
  }
  async function fetchRepresentantes() {
    const representantes = await client.get("/server/representantes", {
      headers: { Authorization: `Bearer ${token}` },
    });
    setRepresentantes(representantes.data.data);
  }

  async function fetchContratista() {
    const contratista = await client.get(`/server/contratistas/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    const contratistaForm = {
      ...contratista.data.data,
      id_representante: contratista.data.data.representante._id,
      id_especialidad: contratista.data.data.especialidad._id,
    };
    form.setFieldsValue(contratistaForm);
  }

  useEffect(() => {
    fetchEspecialidades();
    fetchRepresentantes();
    fetchContratista();
  }, []);

  return (
    <Row>
      <Col span={8} offset={2}>
        <Form
          onFinish={editContratista}
          form={form}
          {...layout}
          layout="horizontal"
        >
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Rut"
            name="rut"
            rules={[{ required: true, message: "Ingrese el rut" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Especialidad"
            name="id_especialidad"
            rules={[{ required: true, message: "Seleccione una especialidad" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Especialidades"}
                />
              }
            >
              {especialidades.map((especialidad) => {
                return (
                  <Select.Option
                    key={especialidad._id}
                    value={especialidad._id}
                  >
                    {especialidad.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label="Representante"
            name="id_representante"
            rules={[{ required: true, message: "Seleccione un representante" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Representantes"}
                />
              }
            >
              {representantes.map((representante) => {
                return (
                  <Select.Option
                    key={representante._id}
                    value={representante._id}
                  >
                    {representante.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
export default EditarContratista;
