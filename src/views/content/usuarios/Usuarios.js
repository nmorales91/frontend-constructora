import React, { useState, useEffect } from "react";
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  AutoComplete,
  Button,
  Alert,
  Empty,
} from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import autoCompleteEmail from "../../../utils/autocompleteEmail";
import client from "../../../client/client";

const Usuarios = () => {
  const initialState = {
    correo: "",
    clave: "",
    nombre: "",
    apellido_paterno: "",
    apellido_materno: "",
    rol: "",
  };

  const [form] = Form.useForm();

  const [result, setResult] = useState([]);
  const [roles, setRoles] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const handleSearch = (value) => {
    setResult(autoCompleteEmail(value));
  };

  const saveUser = (user) => {
    setError(null);
    setSuccess(null);
    client
      .post("/server/usuarios", user)
      .then((response) => {
        setSuccess(`Usuario ${user.correo} agregado con éxito`);
        form.setFieldsValue(initialState);
      })
      .catch((error) => {
        if (error.response.status) {
          setError(error.response.data.message);
        } else {
          setError("Ocurrió un error al agregar el usuario");
        }
      });
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  useEffect(() => {
    async function fetchRoles() {
      const roles = await client.get("/server/roles");
      setRoles(roles.data.data);
    }
    fetchRoles();
  }, []);

  return (
    <Row>
      <Col span={8} offset={2}>
        <Form
          onFinish={saveUser}
          initialValues={initialState}
          form={form}
          {...layout}
          layout="horizontal"
        >
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Apellido Paterno"
            name="apellido_paterno"
            rules={[{ required: true, message: "Ingrese un apellido" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Apellido Materno" name="apellido_materno">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Correo"
            name="correo"
            rules={[{ required: true, message: "Ingrese su correo" }]}
          >
            <AutoComplete onSearch={handleSearch} options={result}>
              <Input autoComplete="off" />
            </AutoComplete>
          </Form.Item>
          <Form.Item
            label="Clave"
            name="clave"
            rules={[{ required: true, message: "Ingrese una clave" }]}
          >
            <Input.Password
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <Form.Item
            label="Rol"
            name="rol"
            rules={[{ required: true, message: "Seleccione un rol" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Roles"}
                />
              }
            >
              {roles.map((rol) => {
                return (
                  <Select.Option key={rol._id} value={rol.nombre}>
                    {rol.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
export default Usuarios;
