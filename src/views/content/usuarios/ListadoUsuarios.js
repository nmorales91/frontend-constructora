import React, { useEffect, useState } from "react";
import client from "../../../client/client";
import { Table } from "antd";
import { Link } from "react-router-dom";

const columns = [
  {
    title: "Nombre",
    dataIndex: "nombre",
    key: "nombre",
    render: (text, record) => {
      return `${text} ${record.apellido_paterno}`;
    },
  },
  { title: "Correo", dataIndex: "correo", key: "correo" },
  { title: "Rol", dataIndex: "rol", key: "rol" },
  {
    title: "Último Ingreso",
    dataIndex: "ultimo_ingreso",
    key: "ultimo_ingreso",
    render: (fecha) => {
      let date = new Date(fecha);
      return <span>{date.toLocaleString("es-CL", { timeZone: "UTC" })}</span>;
    },
  },
  {
    title: "Editar",
    dataIndex: "",
    key: "x",
    render: (text, record) => {
      return <Link to={`usuario/${record._id}`}>Editar</Link>
    },
  },
];

const ListadoUsuarios = () => {
  const [usuarios, setUsuarios] = useState([]);
  const [load, setLoad] = useState(true);

  useEffect(() => {
    async function fetchUsuarios() {
      const token = localStorage.getItem("jwt");
      const result = await client.get("/server/usuarios", {
        headers: { Authorization: `Bearer ${token}` },
      });
      setLoad(false);
      setUsuarios(result.data.data);
    }
    fetchUsuarios();
  }, []);

  return <Table loading={load} dataSource={usuarios} columns={columns} />;
};

export default ListadoUsuarios;
