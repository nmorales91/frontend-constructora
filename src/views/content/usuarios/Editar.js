import { useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import autoCompleteEmail from "../../../utils/autocompleteEmail";
import client from "../../../client/client";
import {
  Form,
  Input,
  Select,
  Row,
  Col,
  AutoComplete,
  Button,
  Alert,
  Empty,
} from "antd";

const EditarUsuario = () => {
  const { id } = useParams();
  const token = localStorage.getItem("jwt");

  const [form] = Form.useForm();

  const [result, setResult] = useState([]);
  const [roles, setRoles] = useState([]);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

  const handleSearch = (value) => {
    setResult(autoCompleteEmail(value));
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };

  const editUser = (editUser) => {
    setError(null);
    setSuccess(null);
    client
      .put(`/server/usuarios/${id}`, editUser, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        setSuccess(`Usuario ${editUser.correo} editado con éxito`);
      })
      .catch((error) => {
        if (error.response.status) {
          let errormsg = error.response.data.message;
          if (errormsg.includes("duplicate key")) {
            errormsg = "Este correo ya está siendo utilizado";
          }
          setError(errormsg);
        } else {
          setError("Ocurrió un error al editar el usuario");
        }
      });
  };

  async function fetchRoles() {
    const roles = await client.get("/server/roles");
    setRoles(roles.data.data);
  }

  async function fetchUser() {
    const usuario = await client.get(`/server/usuarios/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    form.setFieldsValue(usuario.data.data);
  }

  useEffect(() => {
    fetchUser();
    fetchRoles();
  }, []);

  return (
    <Row>
      <Col span={8} offset={2}>
        <Form onFinish={editUser} form={form} {...layout} layout="horizontal">
          <Form.Item
            label="Nombre"
            name="nombre"
            rules={[{ required: true, message: "Ingrese un nombre" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Apellido Paterno"
            name="apellido_paterno"
            rules={[{ required: true, message: "Ingrese un apellido" }]}
          >
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item label="Apellido Materno" name="apellido_materno">
            <Input autoComplete="off" />
          </Form.Item>
          <Form.Item
            label="Correo"
            name="correo"
            rules={[{ required: true, message: "Ingrese su correo" }]}
          >
            <AutoComplete onSearch={handleSearch} options={result}>
              <Input autoComplete="off" />
            </AutoComplete>
          </Form.Item>
          <Form.Item label="Clave" name="clave">
            <Input.Password
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
            />
          </Form.Item>
          <Form.Item
            label="Rol"
            name="rol"
            rules={[{ required: true, message: "Seleccione un rol" }]}
          >
            <Select
              notFoundContent={
                <Empty
                  image={Empty.PRESENTED_IMAGE_SIMPLE}
                  description={"No hay Roles"}
                />
              }
            >
              {roles.map((rol) => {
                return (
                  <Select.Option key={rol._id} value={rol.nombre}>
                    {rol.nombre}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ marginBottom: 10, width: "100%" }}
            >
              Guardar
            </Button>
            {error ? <Alert message={error} type="error" /> : null}
            {success ? <Alert message={success} type="success" /> : null}
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default EditarUsuario;
