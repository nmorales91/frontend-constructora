import { Row, Col } from "antd";
import React, { useState, useEffect } from "react";
import FormularioEspecialidades from "../../../components/especialidades/FormularioEspecialidades";
import ListadoEspecialidades from "../../../components/especialidades/ListadoEspecialidades";
import client from "../../../client/client";

const Especialidades = () => {
  const [especialidades, setEspecialidades] = useState([]);
  const [load, setLoad] = useState(false);
  async function fetchEspecialidades(first) {
    if (first) {
      setLoad(true);
    }
    const token = localStorage.getItem("jwt");
    const result = await client.get("/server/especialidades", {
      headers: { Authorization: `Bearer ${token}` },
    });
    if (first) {
      setLoad(false);
    }
    setEspecialidades(result.data.data);
  }

  useEffect(() => {
    fetchEspecialidades(true);
  }, []);

  return (
    <Row>
      <Col span={8} offset={1}>
        <FormularioEspecialidades fetchEspecialidades={fetchEspecialidades} />
      </Col>
      <Col span={8} offset={3}>
        <ListadoEspecialidades load={load} especialidades={especialidades} />
      </Col>
    </Row>
  );
};

export default Especialidades;
