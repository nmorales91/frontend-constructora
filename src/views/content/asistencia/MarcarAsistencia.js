import React, { useEffect, useState } from "react";
import { Row, Col } from "antd";
import HeaderAsistencia from "../../../components/asistencia/Header";
import Asistencia from "../../../components/asistencia/Asistencia";
import Informacion from "../../../components/asistencia/Informacion";

const MarcarAsistencia = () => {
  const [asistencia, setAsistencia] = useState(null);

  return (
    <>
      <Row>
        <Col span={24}>
          <HeaderAsistencia />
        </Col>
      </Row>
      <Row>
        <Col span={12}>
          <Asistencia setAsistencia={setAsistencia} />
        </Col>
        <Col span={12}>
          <Informacion asistencia={asistencia} />
        </Col>
      </Row>
    </>
  );
};

export default MarcarAsistencia;
