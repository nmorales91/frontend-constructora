import React, { useContext, useState, useEffect } from "react";
import { StoreGlobal } from "../context/store/Store";
import {
  setCurrentUser,
  logoutUser,
} from "../context/actions/autenticacion.action";
import { useHistory } from "react-router-dom";
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Typography,
  Alert,
  Spin,
  AutoComplete,
} from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { getColor } from "../utils/utils";
import { environment } from "../config/config";
import jwt_decode from "jwt-decode";
import client from "../client/client";
import autoCompleteEmail from "../utils/autocompleteEmail";

const { Title } = Typography;

const Login = () => {
  const context = useContext(StoreGlobal);

  // redirect auth
  let history = useHistory();
  useEffect(() => {
    if (context.stateUser.isAuthenticated) {
      history.push("/home");
    }
  }, [context.stateUser.isAuthenticated, history]);
  // redirect auth

  const initialState = {
    correo: "",
    clave: "",
  };

  const [error, seterror] = useState(null);
  const [load, setload] = useState(false);
  const [form] = Form.useForm();

  const [result, setResult] = useState([]);

  const handleSearch = (value) => {
    setResult(autoCompleteEmail(value));
  };

  const loginUser = (user) => {
    const { correo, clave } = user;
    seterror(null);
    setload(true);
    client
      .post("/server/login", {
        correo,
        clave,
      })
      .then((response) => {
        if (response.data.data) {
          jwtToken(response.data.data.token);
        } else {
          seterror(response.data.message);
          logoutUser(context.dispatch);
        }
        setload(false);
        form.setFieldsValue(initialState);
      })
      .catch((error) => {
        seterror(error.message);
        setload(false);
        logoutUser(context.dispatch);
        form.setFieldsValue(initialState);
      });
  };

  const jwtToken = (token) => {
    localStorage.setItem("jwt", token);
    const decoded = jwt_decode(token);
    seterror(null);
    context.dispatch(setCurrentUser(decoded));
  };

  return (
    <Row className="app" style={{ backgroundColor: getColor(environment.env) }}>
      <Col span={4}>
        <Title level={2} style={{ textAlign: "center" }}>
          Tesis {environment.env}
        </Title>
        <Spin spinning={load}>
          <Form onFinish={loginUser} initialValues={initialState} form={form}>
            <Form.Item
              name="correo"
              rules={[{ required: true, message: "Ingrese su correo" }]}
            >
              <AutoComplete onSearch={handleSearch} options={result}>
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Username"
                  autoComplete="off"
                />
              </AutoComplete>
            </Form.Item>

            <Form.Item
              name="clave"
              rules={[{ required: true, message: "Ingrese su clave" }]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Password"
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                style={{ marginBottom: 10, width: "100%" }}
              >
                Ingresar
              </Button>
              {error ? <Alert message={error} type="error" /> : null}
            </Form.Item>
          </Form>
        </Spin>
      </Col>
    </Row>
  );
};

export default Login;
