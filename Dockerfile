FROM node:alpine
WORKDIR /app
COPY . ./
RUN npm i
RUN npm run build

FROM nginx:alpine
COPY --from=0 /app/build /usr/share/nginx/html
COPY .htaccess /usr/share/nginx/html

EXPOSE 80

RUN rm /etc/nginx/conf.d/default.conf
COPY default.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]


